const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  username: {
    type: String,
    require: true,
  },
  userId: {
    type: String,
    required: true,
  },
  totalAmount: {
    type: Number,
    default: 0,
  },
  isDelivered: {
    type: Boolean,
    default: false,
  },
  products: [
    {
      name: {
        type: String,
      },
      productId: {
        type: String,
        required: true,
      },
      quantity: {
        type: Number,
        default: 0,
      },
      subTotal: {
        type: Number,
        default: 0,
      },
    },
  ],
});

module.exports = mongoose.model("Cart", cartSchema);
