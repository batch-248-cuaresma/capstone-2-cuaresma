const { Storage } = require("@google-cloud/storage");

// Path to the service account key file

const keyFilename = "./amiable-anagram-434513-t0-bcdb939b3186.json";

// Initialize Google Cloud Storage
const storage = new Storage({ keyFilename });
const bucket = storage.bucket("ecommerce_docs_bucket");

module.exports = bucket;
