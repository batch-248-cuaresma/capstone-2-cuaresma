const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  cartId: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  totalAmount: {
    type: Number,
    required: true,
  },
  address: {
    type: String,
  },
  status: {
    type: String,
    enum: ["Pending", "Processing", "Shipped", "Delivered", "Cancelled"],
    default: "Pending",
  },
  transactionDone: {
    type: Boolean,
    default: false,
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
  products: [
    {
      name: {
        type: String,
      },
      productId: {
        type: String,
        required: true,
      },
      quantity: {
        type: Number,
        default: 1,
      },
      subTotal: {
        type: Number,
        default: 0,
      },
    },
  ],
  comment: {
    type: String,
  },
});

module.exports = mongoose.model("Order", orderSchema);
