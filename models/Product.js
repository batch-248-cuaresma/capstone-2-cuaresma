const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Product name is required!"],
  },
  category: {
    type: String,
    enum: ["hot", "iced", "snacks"],
    default: "hot",
  },
  subCategory: {
    type: String,
    required: [true, "Category is required!"],
  },
  size: {
    type: String,
    enum: ["short", "tall", "grande", "venti"],
    default: "short",
  },
  imageURL: {
    type: String,
    default: "",
  },
  price: {
    type: Number,
    required: [true, "Price is required!"],
  },
  isAvailable: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  totalSales: {
    type: Number,
    default: 0,
  },
  starRate: {
    type: Number,
  },
  order: [
    {
      orderId: {
        type: String,
        required: true,
      },
      quantity: {
        type: Number,
        required: true,
      },
      subTotal: {
        type: Number,
        default: 0,
      },
      purchasedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
});

module.exports = mongoose.model("Product", productSchema);
