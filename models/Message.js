const mongoose = require("mongoose");

const messageSchema = new mongoose.Schema({
  to: {
    type: String,
    require: true,
  },
  from: {
    type: String,
    require: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  message: {
    type: String,
    require: true,
  },
});
