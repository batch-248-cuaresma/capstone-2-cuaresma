const mongoose = require("mongoose");
const Cart = require("./Cart");
const { Object } = require("mongoose/lib/schema/index");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [false, "Frist name is required"],
  },
  lastName: {
    type: String,
    required: [false, "Last name is required"],
  },
  username: {
    type: String,
    required: [true, "Username is required"],
  },
  address: {
    type: String,
  },
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  mobileNo: {
    type: String,
    required: [false, "Mobile number is required"],
  },
  otp: {
    type: Number,
    required: [false, "OTP number is required"],
  },
  userType: {
    type: String,
    enum: ["client", "admin"],
    default: "client",
  },
  cartId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: Cart,
    required: [false],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  isEmailVerify: {
    type: Boolean,
    default: false,
  },
  isForgotPassword: {
    type: Boolean,
    default: false,
  },
  customs: {
    type: Object,
    default: {},
  },
});

module.exports = mongoose.model("User", userSchema);
