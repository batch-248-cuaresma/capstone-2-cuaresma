const multer = require("multer");
const path = require("path");
const fs = require("fs");

const upload = multer({
  storage: multer.memoryStorage(),
  limits: { fileSize: 10 * 1024 * 1024 },
});

module.exports = upload;
