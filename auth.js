const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const secret = "eCommerceAPI-Milk-tea";

// Create and return an access token for the given user details
module.exports.createAccessToken = (userDetails) => {
  const data = {
    // Create a data object with the user details to be included in the access token
    id: userDetails.id,
    email: userDetails.email,
    userType: userDetails.userType,
    cartId: userDetails.cartId,
    isActive: userDetails.isActive,
  };

  // Sign the data object with the secret key and return the resulting access token
  return jwt.sign(data, secret, {});
};

module.exports.createLink = async (userDetail) => {
  try {
    const data = {
      id: userDetail.id,
      email: userDetail.email,
    };
    const accessToken = jwt.sign(data, secret, { expiresIn: "24h" });

    const transport = nodemailer.createTransport({
      host: "smtp.gmail.com",
      secure: true,
      auth: {
        user: "jhoycuaresma09@gmail.com",
        pass: "dcnqzcfyqijasleh",
      },
    });

    const mailOptions = {
      from: "jhoycuaresma09@gmail.com",
      to: `${userDetail.email}`,
      subject: "Verify your email address",
      text: `Thank you for registering with our website
			link: http://localhost:4000/users/login/${accessToken}`,
    };
    try {
      await transport.sendMail(mailOptions);
      return "Email successfully sent";
    } catch (error) {
      throw "Error sending email";
    }
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    throw error;
  }
};

// Verify the access token in the request headers and set the user object in the request object
module.exports.verify = (req, res, next) => {
  // Retrieve the access token from the Authorization header in the request
  let token = req.headers.authorization;

  if (typeof token !== "undefined") {
    // Remove the "Bearer " prefix from the access token
    token = token.slice(7);
    // Verify the access token using the secret key
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        // If the access token is invalid or expired, send a failed authentication response
        return res.send({ auth: "failed" });
      } else {
        // Set the user object in the request object to the data object contained in the access token
        req.user = data;
        // Call the next middleware function in the request chain
        next();
      }
    });
  } else {
    // If no access token is provided, send a failed authentication response
    return res.send({ auth: "failed" });
  }
};

// Verify if the user is an admin
module.exports.verifyAdmin = (req, res, next) => {
  // Check if the user is an admin
  console.log("is user admin", req.user);
  if (req.user.userType == "admin") {
    // If the user is an admin, call the next middleware function in the request chain
    next();
  } else {
    // If the user is not an admin, send a failed authentication response with an error message
    return res.send({
      auth: "Failed",
      message: "Action Forbidden",
    });
  }
};

module.exports.regularUser = (req, res, next) => {
  // Check if the user is an admin
  if (req.user.isAdmin == false) {
    // If the user is an admin, call the next middleware function in the request chain
    next();
  } else {
    // If the user is not an admin, send a failed authentication response with an error message
    return res.send({
      auth: "Failed",
      message: "Action Forbidden (Regular User Only)",
    });
  }
};

// Decode a JWT token and return its payload
module.exports.decode = (token) => {
  if (typeof token !== "undefined") {
    if (token.startsWith("Bearer ")) {
      // Check if a token is present
      token = token.slice(7);
      // Extract the token from the "Bearer " prefix

      return jwt.verify(token, secret, (err, data) => {
        // Verify the token
        if (err) {
          // If the token is invalid or has expired, return null
          return null;
        } else {
          // If the token is valid, decode it and return its payload
          return jwt.decode(token, { complete: true }).payload;
        }
      });
    } else {
      return jwt.verify(token, secret, (err, data) => {
        // Verify the token
        if (err) {
          // If the token is invalid or has expired, return null
          return null;
        } else {
          // If the token is valid, decode it and return its payload
          return jwt.decode(token, { complete: true }).payload;
        }
      });
    }
  } else {
    // If no token is present, return null
    return null;
  }
};
