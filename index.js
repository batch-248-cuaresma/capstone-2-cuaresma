const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;
const cors = require("cors");

mongoose.set("strictQuery", true);
mongoose.connect(
  "mongodb+srv://Adim:1234Admin@clustershop01.uoiifdf.mongodb.net/MTShop?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Hi We are connected to MongoDB Atlas!"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());

const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use("/products", productRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use("/order", orderRoutes);

const cartRoutes = require("./routes/cartRoutes");
app.use("/carts", cartRoutes);

const uploadRoutes = require("./routes/uploadRoutes");
app.use("/upload", uploadRoutes);

app.listen(port, () => {
  console.log(`API is now online on port ${port}`);
});
// http://localhost:4000
