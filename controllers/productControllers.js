const Product = require("../models/Product");
const auth = require("../auth");
const Order = require("../models/Order");

// Add a new product to the database, if the product doesn't already exist
const addProduct = async (req, res) => {
  try {
    const { name, category, subCategory, size, price, imageURL } = req.body;
    const isProductExist = await Product.findOne({ name, size });
    const product = new Product({
      name,
      category,
      subCategory,
      size,
      price,
      imageURL: !imageURL ? "" : imageURL,
    });

    if (!isProductExist) {
      await product.save();
      return res.status(201).json({
        status: true,
        message: "You successfully add a new product",
        product,
      });
    }
    return res
      .status(409)
      .json({ status: false, message: "Product is already exist" });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

const allAvailableProduct = async (req, res) => {
  try {
    const product = await Product.find({ isAvailable: true });
    return res
      .status(200)
      .json({ message: "Here are all available Product", product });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

const allProduct = async (req, res) => {
  try {
    const product = await Product.find({});
    return res
      .status(200)
      .json({ message: "Here are all the Product", product });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

const getProduct = async (req, res) => {
  try {
    const id = req.params.id;
    const product = await Product.findOne({ _id: id });
    if (!product || product.isAvailable == false) {
      return res.status(404).json({ message: "Product not available." });
    }
    return res.status(200).json({ message: "Product", product });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

const updatePrice = async (req, res) => {
  try {
    const id = req.params.id;
    const product = await Product.findOne({ _id: id });
    if (!product) {
      return res
        .status(404)
        .json({ status: false, message: "404 Product not found" });
    }
    product.price = req.body.price;
    await product.save();
    return res.status(200).json({
      status: true,
      message: "Successfully changed product price",
      product,
    });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ Status: false, message: "Failed to retrieve the user data." });
  }
};

const updateName = async (req, res) => {
  try {
    const name = req.body.name;
    const id = req.params.id;
    const product = await Product.findOne({ _id: id });
    if (!product) {
      return res
        .status(404)
        .json({ status: fales, message: "Product not found" });
    }
    const productNameExist = await Product.find({
      name: name,
      size: product.size,
    });

    if (productNameExist == "" || productNameExist == null) {
      product.name = name;
      await product.save();
      return res.status(200).json({
        status: true,
        message: "Successfully changed product name",
        product,
      });
    }
    return res
      .status(409)
      .json({ status: false, message: "Prooduct name exist", product });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

// Archive a product by setting its isAvailable flag to false
const archiveProduct = async (req, res) => {
  try {
    const id = req.params.id;
    console.log(id);
    const product = await Product.findOne({ _id: id });
    if (!product) {
      return res
        .status(404)
        .json({ status: false, message: "Product not found" });
    }
    product.isAvailable = false;
    await product.save();
    return res.status(200).json({
      status: true,
      message: "You successfully archive product",
      product,
    });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

// Unarchive a product by setting its isAvailable flag to true
const unarchiveProduct = async (req, res) => {
  try {
    const id = req.params.id;
    const product = await Product.findOne({ _id: id });
    if (!product) {
      return res.status(404).json({ message: "Product not found" });
    }
    product.isAvailable = true;
    await product.save();
    return res
      .status(200)
      .json({ message: "You successfully unarchive product", product });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

const productSale = async (req, res) => {
  try {
    const product = await Product.find({});
    const productData = product.map((products) => {
      return { name: products.name, totalSales: products.totalSales };
    });

    return res
      .status(200)
      .json({ message: "You successfully unarchive product", productData });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

const addStarRate = async (req, res) => {
  try {
    const id = req.params;
    const { star } = req.body;
    const product = await Product.findOne({ id: id });

    if (!product) {
      return res
        .status(404)
        .json({ status: false, message: "Product not found" });
    }

    product.starRate += star;
    await product.save();
    return res.status(200).json({
      status: true,
      message: `You added ${star} out of 5 star Rate`,
      product,
    });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

const addURL = async (req, res) => {
  try {
    console.log("this product", req.body, req.params);
    const id = req.params;
    const { url } = req.body;
    const product = await Product.findOne({ id: id });

    if (!product) {
      return res
        .status(404)
        .json({ status: false, message: "Product not found" });
    }

    product.imageURL = url;
    await product.save();
    return res.status(200).json({
      status: true,
      message: "You adde image url for this product",
      product,
    });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

// Export the  function
module.exports = {
  addProduct,
  allAvailableProduct,
  allProduct,
  getProduct,
  updatePrice,
  updateName,
  archiveProduct,
  unarchiveProduct,
  productSale,
  addStarRate,
  addURL,
};
