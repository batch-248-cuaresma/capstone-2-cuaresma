const File = require("../models/File");
const bucket = require("../models/googleAPI");
const axios = require("axios");

const uploadPDF = async (req, res) => {
  try {
    if (!req.file) {
      return res.status(400).json({ message: "No file uploaded." });
    }

    const { buffer, originalname } = req.file;

    // Save file metadata to MongoDB
    const file = new File({
      filename: req.file.filename,
      path: req.file.path,
      size: req.file.size,
    });
    const fileUrl = `${req.protocol}://${req.get("host")}/uploads/${
      req.file.filename
    }`;

    await file.save();

    res.status(200).json({
      message: "File uploaded successfully",
      file: req.file,
      url: fileUrl,
    });
  } catch (err) {
    console.log("upload file error", err);
    res.status(500).json({ message: "Error uploading file." });
  }
};

const getUploadedFile = async (req, res) => {
  try {
    console.log("call time");
    const id = req.params.id;
    console.log("request params", id);
    const file = await File.findOne({ _id: id });
    if (!file) {
      return res.status(404).json({ message: "No File." });
    }
    return res.status(200).json({ message: "file", file });
  } catch (err) {
    console.log("upload file error", err);
    res.status(500).json({ message: "Error uploading file." });
  }
};

const uploadFile = async (req, res) => {
  try {
    console.log("uploadrequest", req.file);
    const { buffer, originalname } = req.file;
    const blob = bucket.file(originalname);
    const blobStream = blob.createWriteStream({
      resumable: false,
    });

    blobStream.on("error", (err) => {
      console.error("Error uploading file:", err);
      res.status(500).send("Error uploading file.");
    });

    blobStream.on("finish", async () => {
      try {
        // Generate a signed URL for the uploaded file
        const expiresAt = Date.now() + 1000 * 60 * 60; // 1 hour from now
        const [url] = await blob.getSignedUrl({
          version: "v4",
          action: "read",
          expires: expiresAt,
        });

        res
          .status(200)
          .send({ message: `File uploaded successfully`, url: url });
      } catch (err) {
        console.error("Error generating signed URL:", err);
        res.status(500).send("Error generating signed URL.");
      }
    });

    blobStream.end(buffer);
  } catch (err) {
    console.error("Error:", err);
    res.status(500).send("Internal Server Error.");
  }
};

module.exports = {
  uploadPDF,
  getUploadedFile,
  uploadFile,
};
