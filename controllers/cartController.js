const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const Cart = require("../models/Cart");
const auth = require("../auth");
const { decode } = auth;

// Function to add a product to the user's cart
const addToCart = async (req, res) => {
  try {
    const id = decode(req.headers.authorization).cartId;
    const { productId, quantity } = req.body;
    const product = await Product.findOne({ _id: productId });
    const cart = await Cart.findOne({ _id: id });
    if (!product || !cart) {
      return res
        .status(404)
        .json({ katop: false, message: "Product or Cart not found", product });
    }
    const isProductExist = await Cart.findOne({
      _id: id,
      "products.productId": productId,
    });
    if (isProductExist) {
      return res
        .status(409)
        .json({ status: false, message: "Product exist", product });
    }

    const { price, name } = product;
    cart.products.push({
      name,
      productId,
      quantity,
      subTotal: quantity * price,
    });
    cart.totalAmount += quantity * price;
    await cart.save();
    return res
      .status(201)
      .json({ status: true, message: "You successfully add product", cart });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

// Update a product in a user's shopping cart
const updateProduct = async (req, res) => {
  try {
    const id = decode(req.headers.authorization).cartId;
    const { productId, quantity } = req.body;
    const cart = await Cart.findOne({
      _id: id,
      "products.productId": productId,
    });
    const product = await Product.findOne({ _id: productId });
    if (!cart || !product) {
      return res
        .status(404)
        .json({ status: false, message: "Product or Cart not found" });
    }

    const cartProduct = cart.products.find((p) => p.productId === productId);
    const { subTotal } = cartProduct;
    const { totalAmount } = cart;
    const { price } = product;
    const updatedTotalAmount = totalAmount - subTotal + quantity * price;

    cartProduct.subTotal = quantity * price;
    cartProduct.quantity = quantity;
    await cartProduct.save();
    cart.totalAmount = updatedTotalAmount;
    await cart.save();

    return res
      .status(200)
      .json({ status: true, message: "Product not found in cart", cart });
  } catch (error) {
    // Handle any errors that occur during the update
    return res.status(500).json({ status: false, message: error.message });
  }
};

const removeProduct = async (req, res) => {
  try {
    const id = decode(req.headers.authorization).cartId;
    const productId = req.params.productId;

    const cart = await Cart.findOne({
      _id: id,
      "products.productId": productId,
    });

    if (!cart) {
      return res
        .status(404)
        .json({ status: false, message: "Product not found in cart" });
    }

    const cartProduct = cart.products.find((p) => p.productId === productId);
    const { totalAmount } = cart;
    const { subTotal } = cartProduct;
    const updatedTotalAmount = totalAmount - subTotal;

    cart.products.remove(cartProduct);
    cart.totalAmount = updatedTotalAmount;
    await cart.save();
    return res
      .status(200)
      .json({ status: true, message: "Successfully removed product", cart });
  } catch (error) {
    // Return error response if an error occurs
    return res.json({
      status: false,
      message: "Error occurred while removing product from cart",
    });
  }
};

const getCart = async (req, res) => {
  try {
    const id = decode(req.headers.authorization).cartId;
    const cart = await Cart.findOne({ _id: id });

    if (!cart) {
      return res.status(404).json({ status: false, message: "Cart not found" });
    }

    return res.status(200).json({ status: true, cart });
  } catch (error) {
    // Return error response if an error occurs
    return res.json({
      status: false,
      message: "Error occurred while removing product from cart",
    });
  }
};

module.exports = {
  addToCart,
  updateProduct,
  removeProduct,
  getCart,
};
