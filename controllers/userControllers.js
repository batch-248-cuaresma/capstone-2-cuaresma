const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const nodemailer = require("nodemailer");
const { decode } = require("jsonwebtoken");
const { findById } = require("../models/Cart");

const createAccount = async (req, res) => {
  try {
    // Extract the required fields from the request body
    const {
      firstName,
      lastName,
      username,
      email,
      mobileNo,
      password,
      cartId,
      userType,
    } = req.body;

    // Hash the password using bcrypt
    const hashedPw = bcrypt.hashSync(password, 12);

    // Check if the username and email already exist in the database
    const isUsernameExist = await User.findOne({ username });
    const isEmailExist = await User.findOne({ email });
    const min = 100000;
    const max = 999999;
    const OTP = Math.floor(Math.random() * (max - min + 1)) + min;
    let user;
    let cart;
    if (userType == "client") {
      cart = new Cart({});
      user = new User({
        firstName,
        lastName,
        username,
        email,
        mobileNo,
        otp: OTP,
        password: hashedPw,
        cartId: cart._id,
        userType: "client",
      });
    }

    if (userType == "admin") {
      user = new User({
        firstName,
        lastName,
        username,
        email,
        mobileNo,
        otp: OTP,
        password: hashedPw,
        userType: "admin",
      });
    }

    if (userType == "seller") {
      user = new User({
        firstName,
        lastName,
        username,
        email,
        mobileNo,
        otp: OTP,
        password: hashedPw,
        userType: "seller",
      });
    }

    // Create a new cart object and a new user object

    // If the username or email already exist in the database, return a 409 status code with an error message
    if (isUsernameExist) {
      return res
        .status(409)
        .json({ status: false, message: "Username is already exists." });
    }
    if (isEmailExist) {
      return res
        .status(409)
        .json({ status: false, message: "Email is already exists." });
    }

    if (!isUsernameExist && !isEmailExist) {
      sendLink({ email, otp: OTP, type: "emailVerification" }).then((data) => {
        if (data) {
          if (userType == "client") {
            user.save();
            cart.username = username;
            cart.userId = user._id;
            cart.save();
          } else {
            user.save();
          }

          return res.status(201).json({
            status: true,
            message: "Congratulations, you have successfully register",
            // verify: await sendEmail(user),
            user,
            cart,
          });
        } else {
          return res
            .status(500)
            .json({ status: false, message: "Failed to create account" });
        }
      });
    }

    // Save the new user and cart objects to the database

    // Return a 201 status code with a success message, the new user object, and the new cart object
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    return res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

// const verifyEmail = async (req, res) => {
//   try {
//     const { username, password, email } = req.body;
//     const userEmail = auth.decode(req.params.accessToken).email;

//     const user = await User.findOne({ username });

//     if (!user) {
//       return res
//         .status(401)
//         .json({ status: false, message: "Invalid username or password" });
//     }
//     if (email != userEmail) {
//       return res
//         .status(403)
//         .json({ status: false, message: "Action Forbiden" });
//     }

//     const isPasswordCorrect = await bcrypt.compare(password, user.password);

//     if (!isPasswordCorrect) {
//       return res
//         .status(401)
//         .json({ status: false, message: "Invalid username or password" });
//     }
//     user.isEmailVerify = true;
//     user.save();
//     // If username and password are correct, create an access token and return it in the response
//     return res.status(200).json({
//       status: true,
//       message: "You have successfully logged in",
//       accessToken: auth.createAccessToken(user),
//       user,
//     });
//   } catch (error) {
//     // If an error occurs, log it and return a 500 status code with an error message
//     console.log(error);
//     res
//       .status(500)
//       .json({ status: false, message: "Failed to retrieve the user data." });
//   }
// };

const transporter = nodemailer.createTransport({
  service: "Gmail",
  host: "smtp.gmail.com",
  port: 465,
  secure: true,
  auth: {
    user: "jhoycuaresma09@gmail.com",
    pass: "iyfhutsscydymtlo",
  },
  tls: {
    rejectUnauthorized: false,
  },
});

const sendLink = async (req, res) => {
  try {
    console.log(" request", req);
    const { email, otp, type } = req;

    // Check if email and link are provided
    if (!email || !otp) {
      //  res.status(400).json({ message: "Email and OTP are required." });
      return false;
    }

    // Define email options
    const mailOptions = {
      from: "jhoycuaresma09@gmail.com",
      to: `${email}`,
      subject: "OTP",
      text: `Hi Customers,\n\nYour One-Time Password (OTP) for accessing your account is: ${otp}\n\nPlease use this code to complete your verification process. This OTP is valid for 5 minutes.\n\nIf you did not request this OTP, please ignore this email.\n\nBest regards,\nAlpha Joy Cuaresma`,
      html:
        type == "emailVerification"
          ? `<html>
      <head>
        <style>
          body {
            font-family: Arial, sans-serif;
          }
          .email-container {
            width: 100%;
            max-width: 600px;
            margin: auto;
            padding: 20px;
            border: 1px solid #ddd;
            border-radius: 8px;
          }
          .otp {
            font-size: 24px;
            font-weight: bold;
            color: #333;
          }
          .footer {
            font-size: 14px;
            color: #777;
            margin-top: 20px;
          }
        </style>
      </head>
      <body>
        <div class="email-container">
          <h2>Hello Customers,</h2>
          <p>Your One-Time Password (OTP) for accessing your account is:</p>
          <p class="otp">${otp}</p>
          <p>Please use this code to complete your verification process. This OTP is valid for 5 minutes.</p>
          <p>If you did not request this OTP, please ignore this email.</p>
          <div class="footer">
            <p>Best regards,<br>Alpha Joy Cuaresma</p>
          </div>
        </div>
      </body>
      </html>`
          : type == "forgotPassword"
          ? `<html>
      <head>
        <style>
          body {
            font-family: Arial, sans-serif;
          }
          .email-container {
            width: 100%;
            max-width: 600px;
            margin: auto;
            padding: 20px;
            border: 1px solid #ddd;
            border-radius: 8px;
          }
          .otp {
            font-size: 24px;
            font-weight: bold;
            color: #333;
          }
          .footer {
            font-size: 14px;
            color: #777;
            margin-top: 20px;
          }
        </style>
      </head>
      <body>
        <div class="email-container">
          <h2>Hello Customers,</h2>
          <p>Your One-Time Password (OTP) for accessing your account is:</p>
          <p class="otp">${otp}</p>
          <p>Please use this code to complete your forgot password process. This OTP is valid for 5 minutes.</p>
          <p>If you did not request this OTP, please ignore this email.</p>
          <div class="footer">
            <p>Best regards,<br>Alpha Joy Cuaresma</p>
          </div>
        </div>
      </body>
      </html>`
          : `<html>
      <head>
        <style>
          body {
            font-family: Arial, sans-serif;
          }
          .email-container {
            width: 100%;
            max-width: 600px;
            margin: auto;
            padding: 20px;
            border: 1px solid #ddd;
            border-radius: 8px;
          }
          .otp {
            font-size: 24px;
            font-weight: bold;
            color: #333;
          }
          .footer {
            font-size: 14px;
            color: #777;
            margin-top: 20px;
          }
        </style>
      </head>
      <body>
        <div class="email-container">
          <h2>Hello Customers,</h2>
          <p>Your One-Time Password (OTP) for accessing your account is:</p>
          <p class="otp">${otp}</p>
          <p>Please use this code to complete your Other process. This OTP is valid for 5 minutes.</p>
          <p>If you did not request this OTP, please ignore this email.</p>
          <div class="footer">
            <p>Best regards,<br>Alpha Joy Cuaresma</p>
          </div>
        </div>
      </body>
      </html>`,
    };

    // Send email
    await transporter.sendMail(mailOptions);
    return true;
    // res.status(200).json({ success: true, message: "Sent email." });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log("error", error);
    return false;
    // res.status(500).json({ message: "Failed to send email." });
    // return false;
  }
};

const verifyUser = async (req, res) => {
  try {
    const id = auth.decode(req.headers.authorization).id;

    const { otp, type } = req.body;
    const user = await User.findOne({ _id: id });

    if (!user) {
      return res.status(500).json({ message: "User not found" });
    }
    if (!type) {
      return res.status(500).json({ message: "type is required" });
    }
    if (user.otp !== otp) {
      return res.status(500).json({ message: "Invalid Otp" });
    }

    if (type == "emailVerification") {
      if (user.isEmailVerify == true) {
        return res.status(400).json({ message: "Email is verified." });
      } else {
        user.isEmailVerify = true;
        user.save();
        return res.status(200).json({ message: "Email is verified.", user });
      }
    }

    if (type == "forgotPassword") {
      user.customs = user.customs || {};
      user.isForgotPassword = true;
      await user.save().then((data) => {
        console.log("user save data", data);
      });
      return res
        .status(200)
        .json({ message: "Forgot password is verified.", user });
    }
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    return res
      .status(500)
      .json({ message: "Failed to retrieve the user data." });
  }
};

const resendOTP = async (req, res) => {
  try {
    const { email, type } = req.body;
    const user = await User.findOne({ email });
    const min = 100000;
    const max = 999999;
    const OTP = Math.floor(Math.random() * (max - min + 1)) + min;

    if (!user) {
      return res.status(500).json({ message: "User not found" });
    }

    if (type == "forgotPassword") {
      sendLink({ email, otp: OTP, type }).then((data) => {
        if (data) {
          user.customs = user.customs || {};
          user.otp = OTP;
          user.isForgotPassword = false;
          user.save();
          return res.status(200).json({ message: "Send to resend OTP." });
        } else {
          return res.status(500).json({ message: "Failed to resend OTP." });
        }
      });
    }

    if (type == "emailVerification") {
      if (user.isEmailVerify == true) {
        return res.status(400).json({ message: "Email is verified." });
      } else {
        sendLink({ email, otp: OTP, type }).then((data) => {
          if (data) {
            user.otp = OTP;
            user.save();
            return res.status(200).json({ message: "Send to resend OTP." });
          } else {
            return res.status(500).json({ message: "Failed to resend OTP." });
          }
        });
      }
    }
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    return res.status(500).json({ message: "Failed to resend OTP." });
  }
};

const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    console.log("user", email);

    // Find user by username
    const user = await User.findOne({ email });
    // If user not found, return error
    if (!user) {
      return res
        .status(401)
        .json({ status: false, message: "Invalid email or password" });
    }
    // if (user.isEmailVerify == false) {
    //   return res
    //     .status(403)
    //     .json({ status: false, message: "Please verify your email" });
    // }

    // Compare password with the hashed password stored in the database
    const isPasswordCorrect = await bcrypt.compare(password, user.password);

    // If password doesn't match, return error
    if (!isPasswordCorrect) {
      return res
        .status(401)
        .json({ status: false, message: "Invalid email or password" });
    }
    user.isActive = true;
    user.save();
    // If username and password are correct, create an access token and return it in the response
    return res.status(200).json({
      status: true,
      // message: "You have successfully logged in",
      accessToken: auth.createAccessToken(user),
      // user,
    });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

const setAddress = async (req, res) => {
  try {
    const id = auth.decode(req.headers.authorization).id;
    const { address } = req.body;
    const user = await User.findOne({ _id: id });
    if (!user) {
      return res.status(404).json({ status: false, message: "User Not Found" });
    }
    user.address = address;
    user.save();
    return res
      .status(200)
      .json({ status: true, message: "Successfuly added address", user });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

const getProfile = async (req, res) => {
  try {
    const id = auth.decode(req.headers.authorization).id;

    const user = await User.findOne({ _id: id });
    if (!user) {
      return res.status(404).json({ status: false, message: "User Not Found" });
    }
    return res.status(200).json({ status: true, user });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};
const changePassword = async (req, res) => {
  try {
    // Get the user id and current/new passwords from the request
    const id = auth.decode(req.headers.authorization).id;
    const { currentPassword, newPassword } = req.body;
    // Find the user by their id
    const user = await User.findOne({ _id: id });
    // If user doesn't exist, return an error
    if (!user) {
      return res
        .status(401)
        .json({ message: "You must be logged in to change your password." });
    }

    // Compare the current password with the stored password hash
    const isPasswordCorrect = await bcrypt.compare(
      currentPassword,
      user.password
    );

    // If the passwords don't match, return an error
    if (!isPasswordCorrect) {
      return res.status(401).json({ message: "Invalid current password" });
    }

    // Hash the new password and update the user's password in the database
    const hashedPw = bcrypt.hashSync(newPassword, 12);
    user.password = hashedPw;
    await user.save();
    // Return a success message with the updated user data
    return res
      .status(201)
      .json({ message: "You password has been successfully changed", user });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

const forgotPassword = async (req, res) => {
  try {
    // Get the user id and current/new passwords from the request
    const { password, username } = req.body;
    // Find the user by their id
    const user = await User.findOne({ username });
    // If user doesn't exist, return an error
    if (!user) {
      return res.status(401).json({ message: "User not found." });
    }

    if (!user.isForgotPassword) {
      return res.status(401).json({ message: "verify otp first" });
    }

    // Hash the new password and update the user's password in the database
    const hashedPw = bcrypt.hashSync(password, 12);
    user.password = hashedPw;
    user.customs = user.customs || {};
    user.isForgotPassword = false;
    await user.save();
    // Return a success message with the updated user data
    return res
      .status(201)
      .json({ message: "You password has been successfully changed", user });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

const deactivateAccount = async (req, res) => {
  try {
    // Decode user ID from the authorization header
    const userId = auth.decode(req.headers.authorization).id;

    // Find the user in the database using the decoded user ID
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return a 404 error
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Set the user's isActive flag to false and save the changes
    user.isActive = false;
    user.save();

    // Return a success message along with the updated user object
    return res
      .status(200)
      .json({ message: "Successfully deactivated account", user });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

const deleteAccount = async (req, res) => {
  try {
    // Get the user data from the JWT token in the headers
    const userData = auth.decode(req.headers.authorization).id;

    // Find the user's cart and user data using their ID
    const cart = await Cart.findOne({ userId: userData });
    const user = await User.findOne({ _id: userData });

    // If either the cart or user data is not found, return a 404 error
    if (!cart || !user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Delete the user's cart and user data
    await Cart.deleteOne({ _id: cart._id });
    await User.deleteOne({ _id: user._id });

    // Return a success message
    return res.status(200).json({ message: "Successfully deleted account" });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

// Function to make a user an admin
const setAdminUser = async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    const user = await User.findOne({ _id: userData.id });
    user.isAdmin = true;
    await user.save();
    return res.status(200).json({ message: "You are now an admin user", user });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

// Retrieve all registered users from the database
const allRegisteredUser = async (req, res) => {
  try {
    // Use the Mongoose 'find' method to retrieve all users from the 'User' collection
    const users = await User.find({ isAdmin: false });
    // Send the retrieved users back in the response
    return res.status(200).json({ message: "Here are all the users", users });
  } catch (error) {
    // If an error occurs, return a 500 Internal Server Error response with the error message
    return res.status(500).json({ success: false, message: error.message });
  }
};

const getAllOrders = async (req, res) => {
  try {
    const orders = await Order.find({});
    return res.status(200).json({ message: "Here are all the oders", orders });
  } catch (error) {
    // If an error occurs, return a 500 Internal Server Error response with the error message
    return res.status(500).json({ success: false, message: error.message });
  }
};

// Export the  function
module.exports = {
  createAccount,
  verifyUser,
  login,
  setAddress,
  getProfile,
  changePassword,
  deactivateAccount,
  deleteAccount,
  setAdminUser,
  allRegisteredUser,
  getAllOrders,
  sendLink,
  resendOTP,
  forgotPassword,
};
