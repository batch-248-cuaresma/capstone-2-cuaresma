const Product = require("../models/Product");
const Order = require("../models/Order");
const auth = require("../auth");
const Cart = require("../models/Cart");
const { decode } = auth;

const orderProduct = async (req, res) => {
  try {
    // Get user data from token
    let id = decode(req.headers.authorization);
    // Find the cart for the user
    const cart = await Cart.findOne({ _id: id.cartId });

    const { address } = req.body;

    // Check if cart is empty
    if (!cart || cart.products.length === 0) {
      return res.status(404).json({ status: false, message: "Cart is empty" });
    }

    // Extract necessary information from cart
    const { userId, username, totalAmount, products, _id } = cart;

    // Find existing order for the user, if any
    const order = await Order.findOne({ cartId: _id });

    // Create a new order if no existing order is found
    if (!order) {
      const newOrder = new Order({
        cartId: _id,
        userId,
        username,
        address,
        totalAmount,
        products,
      });

      // Save the new order to the database
      await newOrder.save();

      // Update product information with the new order
      for (const product of products) {
        const productObj = await Product.findById(product.productId);
        productObj.order.push({
          name: product.name,
          orderId: newOrder._id,
          quantity: product.quantity,
          subTotal: product.subTotal,
          purchasedOn: newOrder.purchasedOn,
        });
        productObj.totalSales += product.subTotal;
        await productObj.save();
      }
      cart.totalAmount = 0;
      cart.products = [];

      await cart.save();

      // Send response with the new order and a success message
      return res
        .status(200)
        .json({ status: true, message: "Place Order", newOrder });
    }

    // If an existing order is found and it has been delivered
    if (order.transactionDone == true) {
      const newOrder = new Order({
        cartId: _id,
        userId,
        username,
        address,
        totalAmount,
        products,
      });

      // Save the new order to the database
      await newOrder.save();

      // Update product information with the new order
      for (const product of products) {
        const productObj = await Product.findById(product.productId);
        productObj.order.push({
          name: product.name,
          orderId: newOrder._id,
          quantity: product.quantity,
          subTotal: product.subTotal,
          purchasedOn: newOrder.purchasedOn,
        });
        productObj.totalSales += product.subTotal;
        await productObj.save();
      }
      cart.totalAmount = 0;
      cart.products = [];

      await cart.save();

      // Send response with the new order and a success message
      return res
        .status(200)
        .json({ status: true, message: "Place Order", newOrder });
    }
    // If an existing order is found and it has not been delivered
    return res
      .status(409)
      .json({ status: false, message: "Order is on the way" });
  } catch (error) {
    // Handle any errors that occur during the process
    return res.status(500).json({ status: false, message: error.message });
  }
};
// const specialOrder = async (req, res) => {};
const allOrder = async (req, res) => {
  try {
    const order = await Order.find({});

    if (!order) {
      return res
        .status(404)
        .json({ status: false, message: "Order's not found" });
    }
    return res.status(200).json({ status: true, message: "Order", order });
  } catch (error) {
    // Handle any errors that occur during the process
    return res
      .status(500)
      .json({ status: false, success: false, message: error.message });
  }
};

const updateStatus = async (req, res) => {
  try {
    const id = req.params.orderId;
    const status = req.body.status;
    const order = await Order.findOne({ _id: id });
    if (!status) {
      return res
        .status(404)
        .json({ status: false, message: "Provide status declaration" });
    }
    if (!order) {
      return res
        .status(404)
        .json({ status: false, message: "Order not found" });
    }
    if (
      order.status === "Pending" ||
      order.status === "Processing" ||
      order.status === "Shipped"
    ) {
      order.status = status;
      await order.save();
      return res
        .status(200)
        .json({ status: true, message: `Order ${order.status}` });
    }
    if (order.status === "Cancelled") {
      return res
        .status(409)
        .json({ status: false, message: `Order ${order.status}` });
    }

    return res
      .status(200)
      .json({ status: true, message: `Order ${order.status}` });
  } catch (error) {
    // Handle any errors that occur during the process
    return res.status(500).json({ status: false, message: error.message });
  }
};

const orderStatus = async (req, res) => {
  try {
    const getOrder = req.body.status;
    const order = await Order.find({ status: getOrder });

    if (order == "") {
      return res.status(404).json({ message: `No ${getOrder} order`, order });
    }

    return res.status(404).json({ message: `All ${getOrder} order`, order });
  } catch (error) {
    // Handle any errors that occur during the process
    return res.status(500).json({ success: false, message: error.message });
  }
};

const transaction = async (req, res) => {
  try {
    // Extract the user ID from the authorization token
    const id = auth.decode(req.headers.authorization).id;
    const orderId = req.params.orderId;

    const order = await Order.findOne({ _id: orderId, userId: id });

    if (!order) {
      return res
        .status(404)
        .json({ status: false, message: "Order not found" });
    }
    if (order.status === "Cancelled") {
      return res
        .status(403)
        .json({ status: false, message: "Action Forbidden" });
    }
    order.transactionDone = true;
    await order.save();

    // Send the updated cart and order information, along with the user ID, in the response
    return res
      .status(200)
      .json({ status: true, message: `Transaction done.`, order });
  } catch (error) {
    // Handle any errors that occur during the process
    return res.status(500).json({ status: false, message: error.message });
  }
};
const adminChangeTrsaction = async (req, res) => {
  try {
    const id = req.params.id;
    const order = await Order.findOne({ _id: id, status: "Cancelled" });
    if (!order) {
      return res
        .status(403)
        .json({ status: false, message: "Action Forbidden" });
    }
    order.transactionDone = true;
    order.save();
    return res
      .status(200)
      .json({ status: true, message: `Transaction done.`, order });
  } catch (error) {
    // Handle any errors that occur during the process
    return res.status(500).json({ status: false, message: error.message });
  }
};
const addComment = async (req, res) => {
  try {
    const { comment } = req.body;
    const id = req.params.id;
    const order = await Order.findOne({ _id: id });
    if (!order) {
      return res
        .status(404)
        .json({ status: false, message: "Order not found" });
    }
    order.comment = comment;
    await order.save();
    return res.status(200).json({
      status: true,
      message: `You successfuly add comment`,
      order,
    });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

const getCurentOrder = async (req, res) => {
  try {
    const id = auth.decode(req.headers.authorization).id;
    const order = await Order.findOne({ userId: id, transactionDone: false });

    if (!order) {
      return res
        .status(404)
        .json({ status: false, message: "No Pending Order" });
    }
    return res
      .status(200)
      .json({ status: true, message: "Current Order", order });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
const getOrderHistory = async (req, res) => {
  try {
    const id = auth.decode(req.headers.authorization).id;
    const order = await Order.find({ userId: id });
    if (!order) {
      return res
        .status(404)
        .json({ status: false, message: "Order not found" });
    }
    return res
      .status(200)
      .json({ status: true, message: "Order History", order });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
const cancleOrder = async (req, res) => {
  try {
    const userId = auth.decode(req.headers.authorization).id;
    const id = req.params.id;

    const { comment } = req.body;
    const order = await Order.findOne({ _id: id });

    if (!comment) {
      return res
        .status(404)
        .json({ status: false, message: "Please provied reason" });
    }
    if (!order) {
      return res
        .status(404)
        .json({ status: false, message: "Order not found" });
    }
    if (order.status === "Processing") {
      return res.status(403).json({
        status: false,
        message: "Can't cancle, order is already processing",
      });
    }
    if (order.transactionDone) {
      return res.status(403).json({
        status: false,
        message: "Can't cancle, transaction already completed ",
      });
    }
    order.comment = comment;
    order.status = "Cancelled";

    await order.save();
    return res
      .status(200)
      .json({ status: true, message: "Order Cancelled", order });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
const getOrder = async (req, res) => {
  try {
    const id = req.params.id;
    const order = await Order.findOne({ id: id });
    if (!order) {
      return res
        .status(404)
        .json({ status: false, message: "Order not found" });
    }
    return res.status(200).json({
      status: true,
      message: `Order`,
      order,
    });
  } catch (error) {
    // If an error occurs, log it and return a 500 status code with an error message
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

module.exports = {
  orderProduct,
  allOrder,
  orderStatus,
  updateStatus,
  transaction,
  adminChangeTrsaction,
  addComment,
  getCurentOrder,
  getOrderHistory,
  cancleOrder,
  getOrder,
};
