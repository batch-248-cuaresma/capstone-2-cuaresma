const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productControllers = require("../controllers/productControllers");

const { verify, verifyAdmin } = auth;

// Add a new product (Admin only)
router.post("/", verify, verifyAdmin, productControllers.addProduct);

// Get all available products
router.get("/", productControllers.allAvailableProduct);

// Get all products (Admin only)
router.get("/all", verify, verifyAdmin, productControllers.allProduct);

// Get a single product by ID
router.get("/:id", productControllers.getProduct);

// Update a product's price (Admin only)
router.patch("/price/:id", verify, verifyAdmin, productControllers.updatePrice);

// Update a product's name (Admin only)
router.patch("/name/:id", verify, verifyAdmin, productControllers.updateName);

router.put("/imageURL/:id", verify, verifyAdmin, productControllers.addURL);

// Archive a product (Admin only)
router.patch(
  "/archive/:id",
  verify,
  verifyAdmin,
  productControllers.archiveProduct
);

// Unarchive a product (Admin only)
router.patch(
  "/unarchive/:id",
  verify,
  verifyAdmin,
  productControllers.unarchiveProduct
);

//Add Star Rate
router.post("/star/:id", verify, productControllers.addStarRate);

module.exports = router;
