// Import the required modules
const express = require('express');
const router = express.Router();
const auth = require('../auth');
const cartControllers = require('../controllers/cartController');

// Destructure the verify and verifyAdmin functions from the auth module
const { verify, verifyAdmin,regularUser} = auth

// Get all cart detail
router.get('/getCart',verify,regularUser,cartControllers.getCart)

// Adding a product to the cart
router.post('/', verify,regularUser, cartControllers.addToCart)

// Update a product in a user's shopping cart
router.put('/update',verify,cartControllers.updateProduct)

//Delete a product in a user's shopping cart
router.put('/remove/:productId',verify,cartControllers.removeProduct)

// Export the router
module.exports = router
