const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderControllers = require("../controllers/orderControllers");

const { verify, regularUser, verifyAdmin } = auth;

//Check out product
router.post("/order", verify, regularUser, orderControllers.orderProduct);

//Get all Order
router.get("/", verify, verifyAdmin, orderControllers.allOrder);

//Get all status order
router.get("/orderStatus", verify, verifyAdmin, orderControllers.orderStatus);

//Update  Status
router.patch(
  "/status/:orderId",
  verify,
  verifyAdmin,
  orderControllers.updateStatus
);

//Update transaction with the user
router.patch(
  "/transaction/:orderId",
  verify,
  regularUser,
  orderControllers.transaction
);

//Admin Change Transaction
router.put(
  "/transaction/admin/:id",
  verify,
  verifyAdmin,
  orderControllers.adminChangeTrsaction
);

//Add Review
router.post("/comment/:id", verify, orderControllers.addComment);

//Get Current Order
router.get("/currentOrder", verify, orderControllers.getCurentOrder);

//Get User order
router.get("/history", verify, orderControllers.getOrderHistory);

//Cancel Order
router.post("/cancle/:id", verify, regularUser, orderControllers.cancleOrder);
//Get Order
router.get("/:id", verify, orderControllers.getOrder);

module.exports = router;
