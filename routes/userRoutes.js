const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userControllers = require("../controllers/userControllers");
const productControllers = require("../controllers/productControllers");

const { verify, verifyAdmin } = auth;

// Register User
router.post("/register", userControllers.createAccount);

// Send Link
router.post("/sendLink", userControllers.sendLink);

// verify User
router.put("/sendLink/email", verify, userControllers.verifyUser);
//resendOTP
router.put("/resendOTP/email", verify, userControllers.resendOTP);

//Get User details
router.get("/details", verify, userControllers.getProfile);

router.put("/fogot-password", userControllers.forgotPassword);

//Verify user email
// router.post('/login/:accessToken',userControllers.verifyEmail);

// Login User
router.post("/login", userControllers.login);

//Set Address
router.post("/address", verify, userControllers.setAddress);

//Change Password
router.put("/password/:id", verify, userControllers.changePassword);

//Deactivate account
router.get("/deactivate", verify, userControllers.deactivateAccount);

//Delete account
router.delete("/delete", verify, userControllers.deleteAccount);

// Set User As Admin
router.put("/isAdmin", verify, userControllers.setAdminUser);

// verify OTP

// Get All Registered Users (Admin Only)
router.get(
  "/userDetails",
  verify,
  verifyAdmin,
  userControllers.allRegisteredUser
);

//Get all Orders
router.get("/orders", verify, verifyAdmin, userControllers.getAllOrders);

//Get product sales (Admin only)
router.get("/sales", verify, verifyAdmin, productControllers.productSale);

module.exports = router;
