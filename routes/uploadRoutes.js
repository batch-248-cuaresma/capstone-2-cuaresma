const express = require("express");
const router = express.Router();
const auth = require("../auth");
const uploadControllers = require("../controllers/uploadController");
const upload = require("../models/Upload");

router.post("/", upload.single("file"), uploadControllers.uploadPDF);

router.get("/file/:id", uploadControllers.getUploadedFile);

router.post("/upload", upload.single("file"), uploadControllers.uploadFile);

module.exports = router;
